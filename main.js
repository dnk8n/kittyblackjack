function getIsLocalStorage() {
  try {
    localStorage;
    console.debug("Local Storage Supported by Browser");
    return true;
  } catch {
    console.warn("Local Storage not Supported by Browser");
    return false;
  }
}

var isLocalStorage = getIsLocalStorage();

function newTableRow(kittyPicUrl, kittyName, kittyVal) {
  console.debug(
    `Adding a new table with (${kittyPicUrl}, ${kittyName}, ${kittyVal})`
  );
  let kittySpecs = document.getElementById("kittySpecs");
  let htmlToAppend = `
    <tr>
      <td>
        <a id=${kittyPicUrl} href=${kittyPicUrl} download title="Open kitty pic in new tab" target="_blank">
          <img id="kittyPic" src="${kittyPicUrl}">
        </a>
      </td>
      <td>
        ${kittyName}
      </td>
      <td>
        $${kittyVal}
      </td>
    </tr>
  `;
  if (isLocalStorage) {
    refreshKittySpecs(htmlToAppend);
  } else {
    kittySpecs.innerHTML += htmlToAppend;
  }
}

function refreshInnerHtml(element, localStorageKey, htmlToAppend = null) {
  localStorageInnerHtml = localStorage.getItem(localStorageKey) || "";
  if (htmlToAppend) {
    localStorageInnerHtml += htmlToAppend;
    localStorage.setItem(localStorageKey, localStorageInnerHtml);
  }
  element.innerHTML = localStorageInnerHtml;
}

function refreshKittySpecs(htmlToAppend = null) {
  console.debug(`Refreshing Kitty Specs (htmlToAppend=${htmlToAppend})`);
  if (isLocalStorage) {
    refreshInnerHtml(
      document.getElementById("kittySpecs"),
      "kittySpecs",
      htmlToAppend
    );
  }
}

async function newKitty() {
  console.debug("Requesting a new Kitty.");
  let kittyPicResponse = await fetch(
    "https://api.thecatapi.com/v1/images/search?size=small"
  );
  let kittyPicJson = await kittyPicResponse.json();
  let kittyPicUrl = kittyPicJson[0]["url"];
  let kittyNamesResponse = await fetch("kittynames.json");
  let kittyNamesJson = await kittyNamesResponse.json();
  let kittyName = getRandomKittyName(kittyNamesJson);
  let kittyVal = getKittyValue();
  newTableRow(kittyPicUrl, kittyName, kittyVal);
}

function getRandomKittyName(names) {
  return names[getRandomInt(0, names.length - 1)];
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function getKittyValue() {
  return getRandomInt(1, 11);
}

function getCompetingBid() {
  return getRandomInt(11, 21); // This game is rigged!
}
